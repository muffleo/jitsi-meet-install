# Jitsi VPS install

**Jitsi est un service multi-platerforme, gratuit et open-source de vidéo-conférence sécurisé et complétement configurable.**

## Prérequis
* Serveur virtuel privé Debian
* Nom de domaine


## Sécurisation du VPS

1. Créer des comptes utilisateurs pour l'authentification SSH 
```
aduser username
```

2. Donner les droits administrateurs aux nouveaux comptes  
``` 
usermod -aG sudo username
```

3. Pour chaque compte, générer une clef SSH (dans PuttyGen par exemple)

4. Créer le dossier `.ssh` dans le répértoire personnel des comptes créés précédemment 

5. Créer le fichier `authorized_keys` dans `home/username/.ssh` et y copier la clef publique.

6. Interdire la connexion SSH en tant que root 

    Dans `/etc/ssh/sshd_config` modifier 
    `#PermitRootLogin yes` en `PermitRootLogin no` 

5. Relancer le service SSH 
```
sudo service ssh reload
```


## Configuration du serveur web

1. Copier le nom de domaine dans /etc/hostname
    
2. Copier le nom de domaine dans /etc/hosts, à côté de "localhost" 
```
127.0.0.1      localhost       meet.exemple.ch
```
3. Installer Ngnix et un outil pour les certificats Let's Encrypt 
```
sudo apt update && sudo apt install nginx python3-certbot-nginx    
```
4. Générer un certificat Let's Encrypt pour sécuriser le site
```
sudo certbot --nginx -d meet.example.ch
```
5. Copier les informations du certificat dans un endroit sûr

6. Supprimer le fichier `/etc/nginx/sites-enabled/default`

## Installation de Jitsi Meet

1. Ajouter le dépôt des paquets Jitsi
```
echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list  
 
wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add -
```
2. Ouvrir les ports du firewall et l'activer
```
ufw allow ssh  
ufw allow https   
ufw allow 10000/udp  
ufw enable 
``` 

3. Installer Jitsi Meet
```
apt-get install apt-transport-https  
apt-get update  
apt-get -y install jitsi-meet
```
4. Renseigner le FQDN de votre domaine    
![screenshot de l'installation Jitsi](https://i.imgur.com/Cnblg2L.png) 
    
5. Choisir "use my own certificat" et renseigner le chemin du certificat copié lors de l'installation de Nginx
![screenshot de l'installation Jitsi](https://i.imgur.com/LSGbOLh.png) 
## Configuration du secure domain

**Les étapes ci-dessous décrivent comment activer l'authentification pour votre serveur Jitsi. Les utilisateurs devront avoir un compte enrengistré pour commencer une conférence.**

1. Activer l'authentification en modifiant `/etc/prosody/conf.avail/[hostname].cfg.lua`
```
VirtualHost "jitsi-meet.example.com"
    authentication = "internal_plain"
```
2. Ajouter un hôte virtuel pour les invités dans `/etc/prosody/conf.avail/[hostname].cfg.lua`
```
VirtualHost "guest.jitsi-meet.example.com"
    authentication = "anonymous"
    c2s_require_encryption = false
```
3. Ajouter le "anonymousdomain" dans `/etc/jitsi/meet/[hostname]-config.js`
```
var config = {
    hosts: {
            domain: 'jitsi-meet.example.com',
            anonymousdomain: 'guest.jitsi-meet.example.com',
            ...
        },
        ...
}
```
4. Ajouter la  ligne suivane dans le fichier `/etc/jitsi/jicofo/sip-communicator.properties`
```
org.jitsi.jicofo.auth.URL=XMPP:jitsi-meet.example.com
```
5. Créer des utilisateurs avec la commande suivante
```
prosodyctl register <username> meet.example.ch <password>
```
## Documentation
[Marche à suivre officielle (quick install)](https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md)

[Marche à suivre officielle (secure domain)](https://github.com/jitsi/jicofo#secure-domain)  
    
[Marche à suivre alternative](https://dev.to/noandrea/self-hosted-jitsi-server-with-authentication-ie7)

[Documentation Prosody](https://prosody.im/doc/prosodyctl)